# XConTeg content integrator

The provided classes aim at facilitating object-oriented processing operations
over structured documents. Both SGML and XML documents can be used as a source
and processed using the provided methods. The SGML to XML internal conversion
is handled by [OpenSP](https://openjade.sourceforge.net/)
([binary](https://sourceforge.net/projects/openjade/files/)) or [James Clark's
SP](http://www.jclark.com/sp/)
([binary](http://www.jclark.com/sp/howtoget.htm)). Only the OSX/SX converter is
required. The underlying API is
[ElementTree](https://docs.python.org/3/library/xml.etree.elementtree.html), as
implemented and improved by the [lxml](https://lxml.de/) (dependency) module.


## Use

The proposed methods and attributes are defined as a minimum to perform common
tasks in series upon a structured content. Please note that the error attribute
keeps track of a single task only (the latest one). The full pipeline is
tracked in the log file. Among the proposed methods, `transform()` will iterate
over and return a transformed tree for every provided argument (a path to a
stylesheet). This allows the execution of so-called pipelines to achieve a
result after several successful consecutive transformations.


### `Document` methods and attributes

The main methods and attributes of the Document class are summed up here
for quick reference. See code docstrings for details and updates.

| method       | description |
|:-            |:-           |
| ()           | Initializes a document from an XML string. |
| load()       | Parses a document -either XML or SGML- from a file and loads it into a `root`. |
| store()      | Writes the XML document to a file. |
| find()       | Finds an element by its name or path in the XML tree of the document. |
| validate()   | Checks the document contents for conformity against a model. |
| transform() | Transforms the document using a list of XSL-T sheets. Every successive transformation is applied sequentially to the output of the previous one. The optional parameters apply to all sheets. |

| attribute | description |
|:-         |:-           |
| root      | root of an XML tree as lxml.etree.Element object |
| error     | single string describing a previously failed operation |


### `Document` processing example

```python
from xconteg import Document
from pathlib import Path
# Load an SGML document as XML
document = Document.load(Path("document_0.sgml"))

# check for errors
if document.error is None:

    # apply XSL transformations from stylesheets `a.xsl`, `b.xsl`, `c.xsl`:
    document = document.transform(Path("a.xsl"), Path("b.xsl"), Path("c.xsl"))

# validate the transformations result against a DTD
if document is not None and document.validate(Path("document.dtd")):

    # save the title of the document using a simple XPath expression
    title = t.text if (t := document.find(".//title")) is not None else "?"

    # store the final document named against its title
    document.store(Path(f"{title}.xml"))
```

## Test / Build / Install

The provided [pyproject.toml](pyproject.toml) should make it easy to build a
[distribution package](https://packaging.python.org/en/latest/tutorials/packaging-projects/). Please run the provided tests first.

| what    | how |
|:---     |:--  |
| test    | `python -m unittest` |
| build   | `pip wheel .` or `python -m build` |
| install | `pip install xconteg-x.y.z-py3-none-any.whl` where `x.y.z` is the package version. |
