"""make an XHTML document out of a PDF document"""
import pathlib
try: import pymupdf
except ImportError as e: print(e," Won't be able to load PDF documents.")
from . import ui, xml

def _head(document: pymupdf.Document) -> xml.ET.Element:
    """wrap PDF metadata into an HTML head"""
    root = xml.ET.Element("head")
    title = xml.ET.Element("title")
    title.text = document.metadata.get("title")
    coding = xml.ET.Element("meta")
    coding.attrib.update({"http-equiv":"Content-type",
                          "content":"text/html; charset=UTF-8"})
    for e in (title,coding): root.append(e)
    document.metadata.update({"page_count":document.page_count})
    for k,v in document.metadata.items():
        meta = xml.ET.Element("meta")
        meta.attrib.update({"name":k,"content":str(v) if v else ""})
        root.append(meta)
    return root

def _body(document: pymupdf.Document) -> xml.ET.Element:
    """wrap HTML objects extracted from a PDF into a body"""
    root = xml.ET.Element("body")
    #_parser = LX.XMLParser(recover=True) #False = XMLSyntaxError on chars
    for pn in range(document.page_count):
        page = document[pn]
        xpage: xml.ET.Element
        try:
            xpage = xml.ET.XML(page.get_textpage().extractXHTML())# ,parser=_parser
        except xml.ET.ParseError as e:
            ui.LOG.error(f"{e}, page {page.number+1} ({page}{pn})")
        xpage.attrib.update(({"id":f"page{pn}","class":"page"}))
        xtables = _tables(page)
        if len(xtables) > 0: root.append(xtables)
        root.append(xpage)
    return root

def _tables(page: pymupdf.Page) -> xml.ET.Element:
    """parse HTML tables extracted from a PDF"""
    root = xml.ET.Element("div")
    root.attrib.update({"class":f"tables page{page.number}"})
    for n,table in enumerate(page.find_tables()):
        htable = table.to_pandas().to_html(index=False, escape=False)
        try:
            xtable = xml.ET.XML(htable)
            if xtable.find(".//tbody") is not None: root.append(xtable)
        except xml.ET.ParseError as e:
            ui.LOG.error(f"{e}, page {page.number+1}, table {n+1}")
    return root

def load(source: pathlib.Path) -> tuple[bytes|None, str|None]:
    """Load PDF as html bytestream from file '{}'."""
    result: bytes|None = None
    error: str|None = None
    root = xml.ET.Element("html",xmlns="http://www.w3.org/1999/xhtml")
    try:
        with pymupdf.Document(filetype="pdf",filename=source) as document:
            for wrap in _head,_body: root.append(wrap(document))
    except Exception as e:
        error = str(e)
    if error is None and root is not None:
        #result, error = xml.xslt(root, XSLT_DIR / "mupdf-xhtml.xsl")
        doctype = """<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
            "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">"""
        result = xml.ET.tostring(root, method="xml", encoding="utf-8")
    return result, error
