"""XML-related wrappers and functions

The consistent way of calling these functions is to capture the output as a
two-item tuple, where the first item is the expected content and the second is
a context error, basically the result of a handled exception.  One of the two
items must be None. The end-user process should reliably display the error when
the expected content is None.
"""
from . import ui
import pathlib
import re
import typing
try:
    from saxonche import PySaxonProcessor, PySaxonApiError
    SAXON_AVAILABLE = True
except ImportError as e:
    SAXON_AVAILABLE = False
try:
    import lxml.etree as LX
    LXML_AVAILABLE = True
except ImportError as e:
    LXML_AVAILABLE = False
import xml.etree.ElementTree as ET

NCNAMES_RE = re.compile(r"^[a-zA-Z_][\w\.-]*(:[a-zA-Z_][\w\.-]*)?$")
OPENING_TAG_RE = re.compile(
    r"<((?P<prefix>[a-zA-Z_]?[\w\.-]+):)?(?P<local_name>[a-zA-Z_]?[\w\.-]+).*>")

def _log_headline(log) -> str|None:
    """Format the first line of an error_log, i.e. the main error.

    see https://lxml.de/parsing.html#error-log
    
    """
    log = log.filter_from_errors()[0]
    hint = f" near line {log.line}, col {log.column}" if (log.line and
                                                          log.column) else ""
    return f"{log.message}{hint}"

def parse(source: bytes|str) -> tuple[LX._Element|None,str|None]:
    """use LXML to parse contents"""
    #TODO: return tuple[ET.Element|None, str|None]
    #TODO: remove the lxml dependency
    result, error, parser = None, None, LX.XMLParser()
    try:
        result = LX.XML(source,parser)
        # could process XInclude from here
    except LX.LxmlError:
        error = _log_headline(parser.error_log)
    return result, error

def read_root(source: str) -> str|None:
    """look for an opening XML tag in contents"""
    return OPENING_TAG_RE.match(source).groupdict().get("local_name")

def encode(source:LX._Element, encoding="US-ASCII", **kwargs) -> bytes:
    """Return a bytes view of an XML element"""
    #TODO: remove the lxml dependency
    #TODO: recode parametric output functions
    #TODO: use xml.etree.ElementTree.indent for pretty-printing
    result:bytes|str = b""
    result = LX.tostring(source, encoding=encoding, **kwargs)
    return result.encode(encoding) if isinstance(result,str) else result

def find(source, expression:str, namespaces={}
         ) -> tuple[LX._Element|None,str|None]:
    """Find the first occurrence of an element."""

    def element(x:LX._Element) -> LX._Element|None:
        """get the first result of iterfind"""
        for i in x.iterfind(expression, namespaces=namespaces):
            if i is not None:
                return i
        return None

    result, error = None, None
    try:
        result = element(source)
    except LX.LxmlError as e:
        error = f"{e}: {expression}"
    except SyntaxError as e:
        error = f"'{expression}': {e}."
    return result, error

def validate(source:LX._Element, model:pathlib.Path, full_log:bool=False
             ) -> str|None:
    """use a model to validate an XML"""
    error = None
    # attempt model parsing
    if not all((model.exists(), model.stat().st_size>0)):
        return f"'{model.name}' is empty or missing."
    # then attempt actual validation
    type = model.suffix.lower()
    if type in (".dtd",".rng",".sch",".xsd"):
        validator:LX.DTD|LX.XMLSchema|LX.RelaxNG|None = None 
        match type: #TODO: add ISO Schematron support
            case ".dtd":
                validator = LX.DTD(model)
            case ".xsd":
                validator = LX.XMLSchema(LX.parse(model))
            case ".rng":
                validator = LX.RelaxNG(LX.parse(model))
            case _:
                error = f"Validation is not supported for {type}."
        if validator and validator.validate(source) is False:
            error = " | ".join(
                (str(i) for i in validator.error_log)) if full_log else (
                    _log_headline(validator.error_log))
    return error

def xpath(source, expression:str, namespaces={}):
    """query input with an XPath expression"""
    if isinstance(source,LX._Element):
        # make sure xml is an etree
        source = LX.ElementTree(source)
    return source.xpath(expression, namespaces=namespaces)

def xslt(source:bytes, sheet:pathlib.Path, **params
         ) -> tuple[bytes|None, str|None]:
    """apply an XSL Transformation from a file"""
    if not all((sheet.exists(), sheet.stat().st_size>0)):
        return None, f"'{sheet.name}' is empty or missing."
    return xslt_lxml(source,sheet,**params)

def xslt_lxml(source:bytes, sheet:pathlib.Path, **params
              ) -> tuple[bytes|None, str|None]:
    """use LXML to apply an XSL Transformation from a file"""
    params = {k:LX.XSLT.strparam(v) for k,v in params.items()}
    result:bytes|None = None
    transform: typing.Callable|None
    xsl, error = parse(sheet.read_bytes())
    if error is not None: return result, error
    if xsl is not None:
        try: transform = LX.XSLT(xsl)
        except Exception as e:
            transform, error = None, str(e)
            return result, error
    xml, error = parse(source)
    if error is not None: return result, error
    if transform is not None:
        try: result = bytes(transform(xml,**params)) or None
        except LX.XSLTApplyError as e:
            # https://lxml.de/xpathxslt.html#error-handling
            error = " ".join(tuple(str(i) for i in (
                type(e).__name__,
                transform.error_log[0].message,
                f"(line {transform.error_log[0].line}, "
                f"col {transform.error_log[0].column}).",
                transform.error_log[1].message,
                e)))
    return result, error

def xslt_saxon(source:bytes, sheet:pathlib.Path, base:pathlib.Path|None=None,
              **params) -> tuple[bytes|None, str|None]:
    """Apply an XSL transformation using Saxon-C"""
    #TODO: manage multiple result documents.
    #https://www.saxonica.com/saxon-c/doc12/html/saxonc.html#PyXsltExecutable-set_capture_result_documents
    result, error = None, None
    with PySaxonProcessor(license=False) as saxon:
        proc = saxon.new_xslt30_processor()
        for k,v in params.items():
            proc.set_parameter(k, saxon.make_string_value(v))
        try:
            xslt = proc.compile_stylesheet(
                stylesheet_file=str(sheet.absolute()))
            source = saxon.parse_xml(xml_text=source.decode("utf-8"))
            result = xslt.transform_to_string(xdm_node=source,
                                            base_output_uri=base.as_uri()
                    ) if base else xslt.transform_to_string(xdm_node=source)
        except PySaxonApiError as e:
            error = e
    return (result.encode("utf-8") if result else None), error

def xslt_libxml(source: bytes, sheet: pathlib.Path, **params
              ) -> tuple[bytes|None, str|None]:
    """let xsltproc transform an input using provided stylesheet and params"""
    messages = ui.code_messages("""
        normal, no argument, too many parameters, unknown option, failed to
        parse the stylesheet, error in the stylesheet, error in one of the
        documents, unsupported xsl:output method, string parameter contains
        both quote and double-quotes, internal processing error, processing
        was stopped by a terminating message, could not write the result to
        the output file""".split(","))
    command = "xsltproc --encoding UTF-8".split()
    for key,value in params.items():
        command.extend(("--stringparam",key,value))
    command.extend((str(sheet.absolute()),"-"))
    result, _err, rcode = ui.subproc(source, *command)
    error = messages[rcode] if rcode != 0 and _err is None else str(_err)
    return result, error

class Element(list):
    """Simple builder of an XML element from a text representation.

    Example:
        E("root", {"attribute":"value"},
            E("child", "first child element"),
            E("grandchildren",
                E("grandchild", "grandchild element"),
                "grandchild text node"),
            "last child text node"
        )
    """

    def validate(self):
        """Check for Element validity."""

        def ncname(name:str) -> bool:
            """Make sure a name is an NCName.

            The NCName value must consist exclusively of letters, digits, and
            the underscore, hyphen, and period. Digits, the hyphen, and the
            period may not be used to start a name.
            """
            return NCNAMES_RE.match(name) is not None

        if not ncname(self.tag):
            raise ValueError(f"'{self.tag}' is not an NCName.")
        for node in self:
            match node:
                case str() | Element():
                    pass
                case _:
                    raise TypeError(f"{type(node)} is not a supported node.")
        for k,v in self.attrib.items():
            if not ncname(k):
                raise ValueError(f"'{k}' is not an NCName.")
            if not isinstance(k,str):
                raise TypeError(
                    f"{type(k)} is not a valid attribute name.")
            if not isinstance(v,str):
                raise TypeError(
                    f"{type(v)} is not a valid attribute value.")
    
    def __init__(self, name:str, *nodes:None|str|dict|typing.Self) -> None:
        """gather nodes together into a new object"""
        self.tag = name.strip()
        self.attrib: dict = {}
        for node in nodes:
            if isinstance(node,dict):
                self.attrib.update(node)
            else:
                self.append(node)
        self.validate()
        self.text = self[0] if (len(self) and type(self[0]) is str) else None
    
    def __str__(self) -> str:
        """generate an XML string"""
        contents = ""
        attrib = " ".join([str(i) for i in (
            f"{k}=\"{v}\"" for k,v in self.attrib.items())])
        if attrib: attrib = " "+attrib
        for node in self: contents += str(node)
        return (f"<{self.tag}{attrib}/>" if contents == ""
                else f"<{self.tag}{attrib}>{contents}</{self.tag}>")
    
    def __call__(self, encoding: None|str = None) -> bytes|str:
        """"""
        view = str(self)
        return view.encode(encoding=encoding) if encoding else view
    
    def grow(self):
        """Split a text-only element into siblings whose name is a singularized
        version of their parent's name."""
        
        def singularize(name):
            """make a singular name from its plural form"""
            return name[:-1] if name[-1].lower()=="s" else name

        singular = singularize(self.tag)
        tree = Element(self.tag)
        if (self.tag is not singular and self.text is not None):
            for name in self.text.split(","):
                tree.append(Element(singular,name.strip()))
            tree.text=None
        else:
            tree.text = self.text
            for child in self:
                if type(child) is Element:
                    tree.append(child.grow())
                else:
                    tree.append(child)
        return tree
