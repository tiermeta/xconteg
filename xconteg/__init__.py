""" """
from importlib import metadata, resources
from pathlib import Path
import typing
from . import pdf, sgml, ui, xml

try:
    __version__ = metadata.version("xconteg")
except metadata.PackageNotFoundError:
    __version__ = "?.?.?"

BASE_DIR = resources.files("xconteg") #type: resources.abc.Traversable
LOG = ui.LOG

class Document:
    """XML document ready for object-oriented chained processing."""

    def __init__(self, source:xml.LX._Element|str|bytes, input_cleanup=False
                 ) -> None:
        """Initialize document with {} source."""
        LOG.info((Document.__init__.__doc__ or "").format(type(source)))
        root: xml.LX._Element|None
        self.error: str|None
        match source:
            case xml.LX._Element():
                root, self.error = source, None
            case str():
                root, self.error = xml.parse(source)
            case bytes():
                if input_cleanup:
                    entities = sgml.Entities.load(
                        path=sgml.ENTITIES_PATH / "control.txt")
                    source = ui.proc(source, entities.remove_from)
                root, self.error = xml.parse(source)
            case _:
                root = None
                self.error = f"unsupported input {type(source)}"
        self._tree = xml.LX.ElementTree(root) if root is not None else None
    
    @property
    def root(self):
        """bind the root of the tree to a property of the document"""
        return self._tree.getroot() if self._tree else None
    
    @classmethod
    def load(cls, source:Path, entities_protection=True, input_cleanup=False
             ) -> typing.Self:
        """Load source from file '{}'."""
        LOG.info((cls.load.__doc__ or "").format(
            source.absolute().relative_to(source.parent.absolute())))
        error: str|None = None
        src_suffix = source.suffix.lower()
        src_bytes: bytes|None = None
        if not all((source.exists(),source.is_file(),source.stat().st_size>0)):
            error = f"'{source}' does not exist or is empty."
        if error is None:
            # select loader based on pathfile suffix
            match src_suffix:
                case ".pdf":
                    src_bytes, error = pdf.load(source)
                case ".sgml" | ".sgm":
                    if entities_protection:
                        LOG.info("Entities protection is ON.")
                    src_bytes, error = sgml.load(source,
                        entities_protection=entities_protection)
                case _:
                    src_bytes = source.read_bytes()
            if src_bytes is not None and input_cleanup:
                entities = sgml.Entities.load(
                    path=sgml.ENTITIES_PATH / "control.txt")
                src_bytes = ui.proc(src_bytes, entities.remove_from)
        document = cls(src_bytes or b"")
        document.error = error or document.error
        if document.error: LOG.error(document.error)
        return document
    
    def store(self, out:Path, entities_protection=False, encoding="utf-8",
              pretty_print=True, xml_declaration=True) -> bool:
        """Store contents into file '{}'."""
        LOG.info((self.store.__doc__ or "").format(out))
        if (result := self.encode(encoding=encoding,
                                  xml_declaration=xml_declaration,
                                  pretty_print=pretty_print
                            ) or None) is not None:
            if entities_protection:
                result = sgml.Entities.switch(result.decode()).encode()
            out.parent.mkdir(parents=True, exist_ok=True)
            out.write_bytes(result)
            return all((out.is_file, out.exists(), out.stat().st_size > 0))
        return False
    
    def encode(self, encoding="US-ASCII", **kwargs) -> bytes:
        """Return a bytes view of the document"""
        result = b""
        if condition := self.root is not None:
            ui.require(condition,"Document must not be empty.")
            result = xml.encode(self.root, encoding=encoding, **kwargs)
        return result
    
    def proc(self, function:typing.Callable, *args, gauge=False, **kwargs
             ) -> bytes|None:
        """Process bytes content with '{}'."""
        LOG.info((self.proc.__doc__ or "").format(function.__name__))
        result = None
        source = self.encode()
        if source is not None:
            result = ui.proc(source, function, *args, gauge=gauge, **kwargs)
        return result
    
    def transform(self, *sheets:Path, **params) -> typing.Optional["Document"]:
        """Apply XSL-Transformation from sheet{}."""

        def _transforms(source:bytes|None, *sheets:Path, **params
                        ) -> tuple[bytes|None, str|None]:
            """Apply a chained transformation for every provided sheet."""
            result = source
            error:str|None = None
            tab = len(str(len(sheets)))
            for i, sheet in enumerate(sheets):
                if result is not None:
                    #human-readable index-based logging
                    if len(sheets)>1:
                        ui.LOG.info(f"{i+1:{tab}d}: '{sheet.name}'")
                    result, error = xml.xslt(result, sheet, **params)
                    ui.require(error is None, error)
            return result, error
        
        _info = self.transform.__doc__ or ""
        LOG.info(_info.format(f": '{sheets[0].name}'") if len(sheets)==1 else (
            _info.format(f"s [{len(sheets)}]").replace(".",":")))
        result = None 
        error = "Document is empty." if self.root is None else None
        if error is None:
            result, error = _transforms(self.encode(),*sheets,**params)
        if error: LOG.error(error)
        return Document(result) if result else None
    
    def find(self, expression:str, namespaces={}) -> xml.LX._Element|None:
        """Find the first occurrence of an element."""
        error = "Document is empty." if self.root is None else None
        if error is None:
            result, error = xml.find(self.root, expression, namespaces)
            if result is not None:
                return result
        if error: LOG.error(error)
        return None

    def xpath(self, expression: str, namespaces={}) -> object|None:
        """Query with advanced XPath expression {}."""
        error = "Document is empty." if self.root is None else None
        result: None|object = None
        if error:
            LOG.error(error)
        else:
            result = xml.xpath(self.root, expression, namespaces)
        return result

    def validate(self, model:Path, full_log:bool=False) -> bool:
        """Validate against model '{}'."""
        error = None
        if self.root is not None:
            LOG.info((self.validate.__doc__ or "").format(model.name))
            error = xml.validate(self.root, model, full_log=full_log)
        else:
            error = "Document is empty."
        if error:
            self.error = error
            LOG.error(error)
        return error is None

class Index(list): #TODO dataclass or NamedTuple
    """index of resources"""

    class Entry(Path):
        """a resource indexed by its path"""

        #TODO: access superclass property 'base' for relative results

        def load(self):
            """return the referenced resource as document"""
            return Document.load(self)
    
    def __init__(self, *paths:Path, suffixes:tuple[str]=("",), recurse=True):
        """Create a series of entries, optionally filtered by suffix."""
        _suffixes = set(s.strip().lower() for s in suffixes if (
            s and s.strip() != ""))
        paths = tuple(p for p in (i.resolve() for i in paths) if p.exists())
        self.base = paths[0] if (len(paths)==1 and paths[0].is_dir()) else None
        if self.base: # generate contents from a single input dir.
            paths = tuple(p for p in (
                self.base.rglob("*") if recurse else self.base.glob("*")
                ) if not p.name.startswith("."))
        for p in paths:
            if len(_suffixes) == 0 or p.suffix.lower() in _suffixes:
                self.append(Index.Entry(p))
