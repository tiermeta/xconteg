<?xml version="1.0" encoding="utf-8"?>
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  
  <!--
    set every name (element or attribute) to upper case.
  -->

  <xsl:output
    method="xml" omit-xml-declaration="yes" encoding="utf-8"
  />

  <xsl:variable name="chars.case.lower"
    >abcdefghijklmnopqrstuvwxyz</xsl:variable>
  <xsl:variable name="chars.case.upper"
    >ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>

  
  <xsl:template name="to.upper.case">
    <xsl:param name="input"/>
    <xsl:value-of
      select="translate($input , $chars.case.lower, $chars.case.upper)" />
  </xsl:template>

  <xsl:template match="text()|processing-instruction()">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="*">
    <xsl:variable name="name">
      <xsl:call-template name="to.upper.case">
        <xsl:with-param name="input" select="name()"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:element name="{$name}">
      <xsl:apply-templates select="@*|node()"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="@*">
    <xsl:variable name="name">
      <xsl:call-template name="to.upper.case">
        <xsl:with-param name="input" select="name()"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:attribute name="{$name}">
      <xsl:value-of select="."/>
    </xsl:attribute>
  </xsl:template>

</xsl:transform>
