<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="h xs">

  <!-- cleanup the XHTML output of pyMuPDF -->
  
  <xsl:output method="xml" indent="no" encoding="UTF-8"/>
  <xsl:strip-space elements="h:* *"/>
  <xsl:preserve-space elements="h:pre pre"/>
  
  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" />
      <!-- default processing -->
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="@xml:space  | @version">
    <!-- remove useless attributes -->
  </xsl:template>
  
  <xsl:template match=" meta[@name = 'http-equiv']">
    <!-- remove useless metadata -->
  </xsl:template>
  
  <!-- tables cleanup -->
  
  <xsl:template match="
    */@rowspan[.='1'] | */@colspan[.='1'] |
    table/@border[.='1'] | tr/@style | table/@class
    ">
    <!-- remove useless table properties -->
  </xsl:template>
  
  <xsl:template match="table[count(tbody/*)=0]">
    <!-- remove empty (i.e. style-only) tables -->
  </xsl:template>
  
</xsl:transform>
