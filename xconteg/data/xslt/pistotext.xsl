<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  
  <xsl:import href="_lib-copy.xsl" />
  
  <xsl:param name="target"/>
  <xsl:param name="seq_start">|[</xsl:param>
  <xsl:param name="seq_end">]|</xsl:param>
  
  <xsl:output method="xml" omit-xml-declaration="yes"/>

  <xsl:template match="processing-instruction()">
    <xsl:choose>
      <xsl:when test="name(.)=$target">
        <xsl:value-of select="concat($seq_start,string(.),$seq_end)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:transform>
