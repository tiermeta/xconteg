"""SGML-related wrappers and functions"""
import dataclasses
import importlib.resources
import io
import pathlib
import re
import typing
from . import ui, xml

ENTITY_RE = re.compile(r"\&(([A-Za-z0-9])+|\#[0-9]+|(\#[Xx])?[0-9A-Fa-f]+);")
ENTITY_RE_NOT_XRESERVED = re.compile(r"\&((?!lt|amp|quot|apos|gt)[^;]*);")
ENTITIES_PATH:pathlib.Path
# mypy made me do this
with importlib.resources.as_file(importlib.resources.files("xconteg")) as dir:
    ENTITIES_PATH = dir / "data" / "entities"

@dataclasses.dataclass(frozen=True)
class Entity:
    """SGML entity as 4-field object

    The Entity fields match the 4-column table loaded from Entities.path.
    
    """
    set: str    # public entity set
    code: int   # Unicode character code
    name: str   # character entity name
    NAME: str   # Unicode character name (UPPER CASE)

    def __eq__(self, other:object) -> bool:
        """Compare with other object.

        First compare names, because they should be unique.
        Compare codes eventually.

        """
        if isinstance(other, Entity):
            return self.name == other.name
        if isinstance(other, str):
            return self.name == other
        if isinstance(other, int):
            return self.code == other
        return False
    
    def __str__(self) -> str:
        """Show the first existing name, code or Unicode name."""
        if self.name:
            return Entity.ref(self.name)
        if self.code:
            return Entity.ref(self.xcode)
        return self.NAME

    def declare(self, notation:str=""):
        declaration = [f"<!ENTITY {self.name}",]
        if notation:
            declaration.append(f"{notation.strip().upper()}")
        declaration.append(f""""{Entity.ref(self.xcode)}">""")        
        return " ".join(declaration)
        
    @property
    def xcode(self) -> str:
        """Show as hexa code."""
        return f"{hex(self.code)}"

    @classmethod
    def _int(cls, value:str) -> int:
        """Convert a string value to an integer"""
        base, value = 10, value.lstrip("#")
        if value.startswith("0x"):
            base = 16
        if value.startswith("x"):
            base, value = 16, f"0{value}"
        return int(value,base)
    
    @classmethod
    def ref(cls, ent:str) -> str:
        """Format an SGML reference to an entity."""

        def is_int() -> bool:
            """Is the entity value an integer?"""
            for base in (16,10):
                try:
                    int(ent,base)
                    return True
                except ValueError: pass
            return False
        
        ent_is_int = is_int()
        if ent.startswith("0x") and ent_is_int:
            return f"&#{ent[1:]};"            
        elif ent_is_int:
            return f"&#{int(ent,10)};"
        else:
            return f"&{ent};"
    
    @classmethod
    def parse(cls, hint:str|int) -> "Entity":
        """Make an incomplete entity from a hint object."""
        _code, _name, _NAME, _set = 0, "", "", ""
        if isinstance(hint,int): _code = hint
        elif hint.startswith("#"):
            _code = cls._int(hint)
        elif hint == hint.upper(): _NAME = hint
        else:
            _name = hint
        return Entity(name=_name, NAME=_NAME, code=_code, set=_set)
    
class Entities(set):
    """a set of entities with utility methods"""

    def __new__(self, *ent:Entity):
        return set.__new__(Entities, *ent)
    
    def get(self, hint:str) -> Entity|None:
        """get the first entity whose code or name matches the hint provided
        as argument."""
        expected = Entity.parse(hint) # incomplete entity
        for entity in self:
            if entity.code == expected.code or entity.name == expected.name:
                return entity
        return None
    
    def rename_in(self, string:str) -> str:
        """Replace codepoints in string with names."""
        for i in ENTITY_RE.findall(string):
            hint = i[0]
            ent = self.get(hint)
            name = ent.name if ent else hint
            string = string.replace(f"{hint}",f"{name}")
        return string
    
    def remove_from(self, string:str) -> str:
        """Remove codepoints from string."""        
        for ent in self:
            string = string.replace(f"{ent.ref(ent.xcode)}","")
        return string
    
    @classmethod
    def switch(cls, source:str,
                  pattern:dict={"start":"<?entity ", "end":" ?>"}) -> str:
        """Switch from/to entities and string pattern in a source string.
        
        Default: "<?entity eacute ?>" from / to "&eacute;"
        Note: SGML PIs end with ">" and are converted to XML Pis ("?>") by SP
        """
        start, end = pattern["start"], pattern["end"]
        protect = lambda x: ENTITY_RE.sub(rf"{start}\1{end}", x)
        unprotect = lambda x:x.replace(start,"&").replace(end,";")
        if "&" in source:
            return protect(source) 
        elif start in source:
            return unprotect(source)
        return source

    def declare(self, notation:str="") -> typing.Iterable:
        """Generate a series of entity declarations."""
        for ent in self:
            yield ent.declare(notation=notation)
    
    @classmethod
    def load(cls, *sets, path:pathlib.Path=ENTITIES_PATH / "custom.txt"):
        """load entities from named sets of the table provided in path"""
    
        _sets = set(s for s in sets) or (
        'ISOamsa', 'ISOamsb', 'ISOamsc', 'ISOamsn', 'ISOamso',
        'ISOamsr', 'ISObox', 'ISOcyr1', 'ISOcyr2', 'ISOdia', 'ISOgrk1',
        'ISOgrk2', 'ISOgrk3', 'ISOgrk4', 'ISOlat1', 'ISOlat2',
        'ISOnum', 'ISOpub', 'ISOtech', 'HTMLspecial', 'HTMLsymbol'
        ) #TODO: dynamically get this list from the user-provided table

        def generate():
            with path.open() as lines:
                """parse a table of entities"""
                for line in (i for i in lines if not i.startswith("#")):
                    if "#" in line:
                        _name_set_code, _NAME = line.strip().split("#")
                        _name, _set, _code = (i.strip() for i in
                                            _name_set_code.split())
                        entity = Entity(code=int(_code,16),
                                        set=_set,
                                        name=_name,
                                        NAME=_NAME)
                        if entity.set in _sets: yield entity
        
        return Entities(generate())

@ui.deprecated("Use the Entities class instead.")
def _ents_dataset(ents_list:pathlib.Path=ENTITIES_PATH / "custom.txt",
                  charsets:tuple=("ISOlat1",)):
    """Create an XML dataset of SGML entities."""
    entities = Entities.load(ents_list)
    dataset = xml.ET.Element("dataset")
    for e in entities(*charsets):
        if e.set in charsets:
            data = xml.ET.Element("data")
            data.set("{http://www.w3.org/XML/1998/namespace}id",e.name)
            data.set("code",e.xcode[1:]) # strip the leading zero
            data.text = chr(e.code)
            dataset.append(data)
    return xml.ET.ElementTree(dataset)

@ui.deprecated("Use Entities.switch(source,pattern) instead.")
def protect_entities(sgmlb:bytes, pattern:dict={"start":"<?entity ","end":" >"}
                     ) -> bytes:
    """Protect entities from expansion in bytestream using a pattern."""
    return switch_entities(sgmlb, pattern=pattern)

@ui.deprecated("Use Entities.switch(source,pattern) instead.")
def unprotect_entities(sgmlb:bytes,
                       pattern:dict={"start":"<?entity ","end":" ?>"})-> bytes:
    """Set entities available again to expansion in bytestream."""
    return switch_entities(sgmlb, pattern=pattern)

@ui.deprecated("Use Entities.switch(source,pattern) instead.")
def switch_entities(sgmlb:bytes,
                    pattern:dict={"start":"<?entity ", "end":" ?>"}) -> bytes:
    """Switch between entities and string pattern in bytestream."""
    return Entities.switch(sgmlb.decode(),pattern=pattern).encode()

def declare_doctype(sgmlb:bytes, doctypes:dict) -> str|None:
    """Declare one of the provided doctypes depending on a document's root.
    The declaration is prependend to the input bytes.    
    {
        "root1":{
            "public":"-//Public Identifier 1",
            "system":"system_file_name",
        },        
        "root2":{
            "public":"-//Public Identifier 2",
            "system":"system_file_name",
        }
    }    
    """
    def declare(root:str) -> str:
        """build declaration from root tag name"""
        dt_public = doctypes[root].get("public")
        dt_system = doctypes[root].get("system")
        quote_str = lambda s: "\"" + s + "\""
        locations = "PUBLIC " + " ".join(
            (quote_str(dt_public), quote_str(dt_system))
            ) if dt_system else quote_str(dt_public)
        if dt_public is None:
            locations = "SYSTEM " + quote_str(dt_system)            
        return f"<!DOCTYPE {root} {locations}>"
    
    # root tag name
    #TODO: use the read_root function from the xml module
    root = io.BytesIO(sgmlb).readline().decode(
        ).split(">")[0].split()[0].lstrip("<")
    return declare(root) if root in doctypes else None

def _log_headline(log:list[bytes]) -> str:
    """Format an sx/osx log entry."""
    entry = log[0].decode("utf-8").strip().split(":")
    return "%s at line %s." % tuple(i.strip() for i in (entry[5],entry[2]))

def load(source: pathlib.Path, entities_protection=True
         ) -> tuple[bytes|None, str|None]:
    """Load SGML contents from file."""
    bytes, out, errors, warning = source.read_bytes(), None, None, None
    if entities_protection:
        bytes = protect_entities(bytes)
    out, errors = parse(bytes, source.parent)
    if errors:
        errlist = io.BytesIO(errors).readlines()
        errors_fp = source.with_suffix(source.suffix+".err")
        with errors_fp.open("wb") as fp:
            for e in errlist: fp.write(e)
        warning = _log_headline(errlist)
    return out, warning

def parse(source:bytes, cwd:pathlib.Path|None=None, exe="osx"
    ) -> tuple[bytes|None,bytes|None]:
    """Use SP to convert SGML bytes to XML bytes.
    
    Captures the standard output and standard error of a subprocess.
    
    The subprocess return code is checked to deal with OSP/OSX strange
    behaviour where no STD output nor STD error is returned. In such
    situation OSX randomly returns an absurd value (3221225477 or so).
    
    The workraound is to run the subprocess again until one of the expected
    codes 0 ('OK') or 1 ('error') is returned. Providing anything else than
    bytes or string as the input seems to ruin the aforementioned logic.
    
    """
    commands = {
        "sx": {
            "args": (
                "-bUTF-8", # force utf-8 output encoding
                "-E0"      # disable error limit
                ),
            "url": "http://www.jclark.com/sp/",
            },
        "osx": {
            "args": (
                "-bUTF-8", # force utf-8 output encoding
                #"-C","catalog", # supposedly implicit
                ),
            "url": "https://openjade.sourceforge.net/"
            },
    }
    args, url = commands[exe]["args"], commands[exe]["url"]
    envvar = {f"{exe}_url":url}
    cmd = [exe]+[i for i in args]
    out, err, rcode = ui.subproc(source, *cmd, cwd=cwd, **envvar)
    if all((out is None, err is None, rcode>1)):
        # Too bad. Try again.
        out, err = parse(source, cwd)
    return out, err
