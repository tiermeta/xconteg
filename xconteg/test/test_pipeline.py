import unittest
import xconteg as XCI

TEST_DIR = XCI.BASE_DIR / "test"
XSLT_DIR = XCI.BASE_DIR / "data" / "xslt"

class PipelineTest(unittest.TestCase):
    """pipeline-related tests"""

    def test_parse_query(self):
        """query a document"""
        source = TEST_DIR / "samples/0.sgml"
        result = XCI.Document.load(source)
        self.assertIsNone(result.error)
        self.assertIsNotNone(result.root)
        elem = result.find(".//*[last()]")
        self.assertIsNotNone(elem)
        self.assertEqual(elem.tag.lower(),'para')
    
    def test_validate(self):
        """validate a document"""
        dtd = TEST_DIR / "samples/0.dtd"
        source = TEST_DIR / "samples/0.sgml"
        result = XCI.Document.load(source)
        result.validate(dtd)
        self.assertIsNotNone(result.error)
        self.assertEqual(result.error,"No declaration for element DOC")
    
    def test_transform(self):
        """transform a document"""
        xsl = XSLT_DIR / "copy.xsl"
        source = TEST_DIR / "samples/0.sgml"
        result = XCI.Document.load(source)
        result = result.transform(xsl)
        self.assertIsNone(result.error)
        self.assertEqual(result.root.tag,"DOC")

    def test_transforms(self):
        """apply chained document transformations"""
        sheets = (XSLT_DIR / xsl for xsl in (
            "copy.xsl",
            "case.xsl",
        ))
        result = XCI.Document("<root />")
        result = result.transform(*sheets)
        self.assertIsNone(result.error)
        self.assertEqual(result.root.tag,"ROOT")

    def test_report_failed_transform(self):
        """let transformation failed with a erroneous sheet"""
        sheet = (TEST_DIR / "test_report_failed_transform.xsl")
        sheet.write_text(
            """<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                 version="1.0">
                <xsl:template match="/">
                  <xsl:value-of select="TIS CANT TRRANSFROM CORECTLI" />
                </xsl:template>
            </xsl:transform>""")
        source, result = XCI.Document("<root />"), None
        with self.assertRaises(SystemExit,
                               msg="Transformation should fail with exit."):
            result = source.transform(sheet)
            self.assertTrue(
                all((result.root.tag == "root", len(result.root) < 1)),
                f"Source {source.root.tag} should not have been transformed.")
        sheet.unlink()

    def test_store(self):
        """store an XML document"""
        source = TEST_DIR / "samples/1.xml"
        result = source.with_suffix(".out.xml")
        document = XCI.Document.load(source)
        self.assertIsNone(document.error, document.error)
        self.assertTrue(document.store(result))
        result.unlink()

    def test_store_element(self):
        """store an XML element as document"""
        source = TEST_DIR / "samples/2.xml"
        result = source.with_suffix(".out.xml")
        root = XCI.Document.load(source).find("PARA")
        document = XCI.Document(root)
        self.assertIsNone(document.error, document.error)
        document.store(result)
        self.assertTrue(result.exists() and result.stat().st_size>0)
        result.unlink()

    def test_prepend_doctype(self):
        """prepend a doctype declaration to a document"""
        source = TEST_DIR / "samples/untyped.xml"
        result = XCI.Document.load(source).encode()
        declaration = XCI.sgml.declare_doctype(
            result, {"doc":{"public":"-//Doc", "system":"doc.dtd"}})
        def prepend_declaration(sgmlb:bytes, declaration:str) -> bytes:
            import io
            result =  io.BytesIO()
            result.write((declaration+"\n").encode())
            result.writelines(io.BytesIO(sgmlb).readlines())
            return result.getvalue()
        self.assertEqual(
            prepend_declaration(result,declaration),
            b"""<!DOCTYPE doc PUBLIC "-//Doc" "doc.dtd">\n<doc>text</doc>""")

    def test_fix_document_contents(self):
        """fix the contents of a document"""
        # 'fixed' output might be SGML, so test against stored bytes
        source_p = TEST_DIR / "samples/xcontents.xml"
        target_p = source_p.with_suffix(".target.xml")
        result_p = source_p.with_suffix(".out.xml")
        result_d = XCI.Document.load(source_p,input_cleanup=True)
        self.assertIsNone(result_d.error)
        self.assertFalse(result_d.root is None)
        # result with entities is saved as bytes, not document
        entities = XCI.sgml.Entities.load()
        result = result_d.proc(entities.rename_in)
        result_p.write_bytes(result)
        target = target_p.read_bytes()
        self.assertEqual(result,target)
        # clean up result if OK
        result_p.unlink()

    def test_xml_from_pdf(self):
        """convert a PDF input to an XHTML output"""
        source = TEST_DIR / "samples" / "hello_world.pdf"
        resultp = source.with_suffix(".out.xml")
        result, error = XCI.pdf.load(source)
        self.assertIsNotNone(result,error)
        with resultp.open("wb") as bf: bf.write(result)
        self.assertTrue(resultp.stat().st_size > 0,
                        "result XML file should not be empty.")
        x = XCI.Document(result)
        #print(x.as_bytes("UTF-8").decode())
        check = x.find(".//h:h1",
                       namespaces={"h":"http://www.w3.org/1999/xhtml"})
        self.assertIsNotNone(check)
        self.assertEqual(check.text,
            "Hello, world! Здравствуйте! καλημέρα 你好 こんにちは 안녕하세요"
        )
        resultp.unlink()

    def _test_entry_from_gui(self):
        """"""
        E = XCI.xml.Element
        root = XCI.ui.TkSimpleInput("Which root?") or "root"
        data = E(str(root),
            E("name","Max"),
            E("surname","Imum"),
            E("items","1,2,3"),
            E("description","none"),
        )
        a = XCI.ui.TkSimpleFields(data)
        print(a)