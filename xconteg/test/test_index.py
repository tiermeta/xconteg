#import importlib.resources
import unittest
import xconteg as XCI

#BASE_DIR = importlib.resources.files("xconteg")
TEST_DIR = XCI.BASE_DIR / "test"

class IndexTest(unittest.TestCase):
    """tests related to the main index"""
    
    def test_list_of_files(self):
        """parse a list of files"""
        index = XCI.Index(TEST_DIR / "samples" / "1.xml",
                          TEST_DIR / "samples" / "2.xml")
        self.assertTrue(len(index)==2)
    
    def test_one_dir(self):
        """automatic index from a single directory"""
        index = XCI.Index(TEST_DIR)
        self.assertTrue(len(index)>1)
    