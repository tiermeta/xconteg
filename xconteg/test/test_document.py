import unittest
import xconteg as XCI

TEST_DIR = XCI.BASE_DIR / "test"

class DocumentTest(unittest.TestCase):
    """tests related to the Document API"""
    
    def test_from_xml_string(self):
        """create a document from an XML string"""
        document = XCI.Document("<root/>")
        self.assertIsNone(document.error, document.error)
        self.assertIsNotNone(document.root, "document is empty")
        self.assertFalse(document.root is None, "document is empty")
    
    def test_syntax_error_in_xml_str(self):
        """capture a syntax error from unparsed XML string"""
        document = XCI.Document("<root></Root>")
        self.assertEqual(document.error,
            "Opening and ending tag mismatch: "
            "root line 1 and Root near line 1, col 14")
        self.assertTrue(document.root is None)
    
    def test_syntax_error_in_xml_file(self):
        """capture a syntax error from unloaded XML file"""
        xml = TEST_DIR / "samples/1.err.xml"
        document = XCI.Document.load(xml)
        self.assertEqual(document.error,"expected '>' near line 3, col 38")
    
    def test_from_xml_file(self):
        """load an XML file as document"""
        xml = TEST_DIR / "samples/1.xml"
        document = XCI.Document.load(xml)
        self.assertFalse(document.root is None)
    
    def test_from_sgml_file(self):
        """load an SGML file to an XML document"""
        source = TEST_DIR / "samples/0.sgml"
        document = XCI.Document.load(source)
        self.assertFalse(document.root is None,
                         "result XML file should not be empty.")
    
    def test_find_element(self):
        """find a single element in the Document tree"""
        result = XCI.Document("""
            <root>
                <level1>
                    <level2>first</level2>
                    <level2 id="2">second</level2>
                </level1>
            </root>
        """).find(".//level2")
        self.assertIsNotNone(result,"no element found")
        self.assertEqual(result.text,"first")
    
