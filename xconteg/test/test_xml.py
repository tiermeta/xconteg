import unittest
import xconteg as XCI

TEST_DIR = XCI.BASE_DIR / "test"
XSLT_DIR = XCI.BASE_DIR / "data" / "xslt"

class XMLTest(unittest.TestCase):
    """XML-related tests"""
    
    def test_syntax_error(self):
        """capture a syntax error from unparsed XML"""
        xml = TEST_DIR / "samples/1.err.xml"
        result, error = XCI.xml.parse(xml.read_bytes())
        self.assertIsNone(result)
        self.assertEqual(error,"expected '>' near line 3, col 38")
    
    def test_from_string(self):
        """parse an XML input from string"""
        xml = "<root/>"
        result, error = XCI.xml.parse(xml)
        self.assertIsNotNone(result, error)
    
    def test_element_class(self):
        """make a str tree from an Element class"""
        E = XCI.xml.Element
        x = E("root", {"xml:id":"attribute_value"},
              E("A", "child value"),
              E("B",
                E("BA", "grandchild value"),
                E("BB", "i think you get the point.")
              ),
              E("C"), # empty element
              "tail value"
            )() # mind the closure
        self.assertEqual(x,
            """<root xml:id="attribute_value"><A>child value</A><B>"""
            "<BA>grandchild value</BA><BB>i think you get the point.</BB>"
            "</B><C/>tail value</root>")
        result, error = XCI.xml.parse(x)
        self.assertIsNotNone(result, error)
    
    def test_element_class_invalid(self):
        """report any invalid Element with an exception"""
        E = XCI.xml.Element
        with self.assertRaises(ValueError) as e:
            x = E("the root", "")
            self.assertEqual(e.exception, "'the root' is not an NCName.")
        with self.assertRaises(ValueError) as e:
            x = E("root",
                    E("1", "")
                )
            self.assertEqual(e.exception, "'1' is not an NCName.")
        with self.assertRaises(ValueError) as e:
            x = E("root",
                    {"1":"invalid NCName"}
                )
            self.assertEqual(e.exception, "'1' is not an NCName.")
        with self.assertRaises(TypeError) as e:
            x = E("root",
                    {"attr":{"sub":""}}
                )
            self.assertEqual(e.exception,
                             "<class 'dict'> is not a valid attribute value.")
        with self.assertRaises(TypeError) as e:
            x = E("root",
                    E("_1", {""})
                )
            self.assertEqual(e.exception,
                             "<class 'set'> is not a supported node.")

    def test_element_class_grow(self):
        """'grow' an empty element"""
        E = XCI.xml.Element
        data = E("profile",
            E("name","Max"),
            E("surname","Imum"),
            E("items","1,2,3"),
            E("description","none"),
        )
        expected=(
            """<profile><name>Max</name><surname>Imum</surname>"""
            """<items><item>1</item><item>2</item><item>3</item></items>"""
            """<description>none</description></profile>""")
        self.assertEqual(data.grow()(),expected)

    def test_from_file(self):
        """parse an XML input from file"""
        xml = TEST_DIR / "samples/1.xml"
        result, error = XCI.xml.parse(xml.read_bytes())
        self.assertIsNotNone(result, error)
    
    def test_transform(self):
        """transform an XML input using a stylesheet"""
        xsl = XSLT_DIR / "copy.xsl"
        source = TEST_DIR / "samples/1.xml"
        result, error = XCI.xml.xslt(source.read_bytes(), xsl)
        self.assertIsNotNone(result, error)
    
    def test_saxon_transform(self):
        """transform an XML input with saxon-c"""
        xsl = XSLT_DIR / "copy.xsl"
        source = TEST_DIR / "samples/1.xml"
        result, error = XCI.xml.xslt_saxon(source.read_bytes(), xsl)
        self.assertIsNotNone(result, error)
    
    def test_libxml2_transform(self):
        """transform an XML input with libxml2"""
        xsl = XSLT_DIR / "copy.xsl"
        source = TEST_DIR / "samples/1.xml"
        result, error = XCI.xml.xslt_libxml(source.read_bytes(), xsl)
        self.assertIsNotNone(result, error)
    