import importlib.resources
import unittest
import xconteg as XCI

BASE_DIR = importlib.resources.files("xconteg")
TEST_DIR = BASE_DIR / "test"

class SGMLTest(unittest.TestCase):
    """SGML-related tests"""

    def test_from_sgml(self):
        """convert an SGML input to an XML output"""
        source = TEST_DIR / "samples/0.sgml"
        xml = source.with_suffix(".out.xml")
        result, _ = XCI.sgml.load(source)
        self.assertIsNotNone(result)
        with xml.open("wb") as bf: bf.write(result)
        self.assertTrue(xml.stat().st_size > 0,
                        "result XML file should not be empty.")
        xml.unlink()
    
    def test_saxon_transform(self):
        """transform an XML input with saxon-c"""
        xsl = BASE_DIR / "data/xslt/copy.xsl"
        source = TEST_DIR / "samples/1.xml"
        result, error = XCI.xml.xslt_saxon(source.read_bytes(), xsl)
        self.assertIsNotNone(result, error)
    
    def test_sgml_errors(self):
        """let sgml errors be logged"""
        source = TEST_DIR / "samples/invalid.sgml"
        result = XCI.Document.load(source)
        self.assertIsNotNone(result.error)
    
    def test_declare_doctype(self):
        """declare a doctype from the root of a document"""
        source = TEST_DIR / "samples/untyped.xml"
        result = XCI.Document.load(source).encode()
        declaration = XCI.sgml.declare_doctype(result,{
        "doc":{
            "public":"-//Public Identifier",
            "system":"dtd.dtd",
        }})
        self.assertEqual(declaration,
            """<!DOCTYPE doc PUBLIC "-//Public Identifier" "dtd.dtd">""")
    
    def test_find_root(self):
        """get the root of a document"""
        source = TEST_DIR / "samples/untyped.xml"
        root_tag_p = XCI.Document.load(source).root.tag # parsed
        root_tag_r = XCI.xml.read_root(source.read_text()) # regexed
        self.assertEqual(root_tag_p,"doc")
        self.assertEqual(root_tag_r,root_tag_p)
    
    def test_sgml_ents_to_pis(self):
        """parse a a preprocessed SGML input"""
        source = TEST_DIR / "samples/entity_to_pi.sgml"
        document = XCI.Document.load(source)
        out = source.with_suffix(".out"+source.suffix)
        self.assertIsNone(document.error)
        self.assertIsNotNone(document.root)
        result = document.root.xpath("//PARA//processing-instruction()")[0]
        self.assertIsNotNone(result)
        document.store(out)
        self.assertEqual(str(result),"<?entity eacute ?>")
        out.unlink()
        
    def test_switch_ents_in_document(self):
        """switch entitites in an SGML input"""
        pattern={"start":"|[","end":"]|"}
        source = TEST_DIR / "samples/entities_to_pattern.xml"
        target = source.with_suffix(".target"+source.suffix).read_text()
        result = XCI.sgml.Entities.switch(source.read_text(),pattern=pattern)
        out = source.with_suffix(".out"+source.suffix)
        out.write_bytes(result.encode())
        self.assertEqual(result,target)
        out.unlink()
    
    def test_sgml_pis_to_ents(self):
        """convert an entity back from a processing instruction"""
        source = """<para><?entity eacute ?></para>"""
        result = XCI.sgml.Entities.switch(source)
        self.assertEqual(result,"<para>&eacute;</para>")
        
    def test_switch_entities_pis(self):
        """switch entities OFF with a PI-pattern"""
        source = """<doc>&eacute;</doc>"""
        pattern = {"start":"<?entity ","end":" ?>"}
        result = XCI.sgml.Entities.switch(source,pattern=pattern)
        self.assertEqual(result,"<doc><?entity eacute ?></doc>")
        # turn entities back ON
        self.assertEqual(
            XCI.sgml.Entities.switch(result,pattern=pattern),source)

    def test_switch_entities_custom(self):
        """switch entities OFF with a custom pattern"""
        source = """<doc>&eacute;</doc>"""
        pattern={"start":"|[","end":"]|"}
        result = XCI.sgml.Entities.switch(source,pattern=pattern)
        self.assertEqual(result,"<doc>|[eacute]|</doc>")
        # turn entities back ON
        self.assertEqual(
            XCI.sgml.Entities.switch(result,pattern=pattern),source)

    def test_named_ents_to_pis(self):
        """replace SDATA named and (hexa)decimal entities"""
        pattern={"start":"<?sdata ","end":"?>"}
        source = " ".join("""
                          du texte &nbsp; du texte 1&gt;0 machin &1; &#Xe9;
                          &gt3; &thinsp; &du texte; &amp; &sperluette
                          |repère 2| &x0eNOTEXA;
                          """.split())
        expected = " ".join("""
                            du texte <?sdata nbsp?> du texte 1<?sdata gt?>0
                            machin <?sdata 1?> <?sdata #Xe9?> <?sdata gt3?>
                            <?sdata thinsp?> &du texte; <?sdata amp?>
                            &sperluette |repère 2| <?sdata x0eNOTEXA?>
                            """.split())
        result = XCI.sgml.Entities.switch(source,pattern=pattern)
        self.assertEqual(result, expected)

    def test_named_ents_to_pattern(self):
        """replace SDATA named and (hexa)decimal entities"""
        pattern={"start":"|[","end":"]|"}
        source = " ".join("""
                          du texte &nbsp; du texte 1&gt;0
                          """.split())
        expected = " ".join("""
                            du texte |[nbsp]| du texte 1|[gt]|0
                            """.split())
        result = XCI.sgml.Entities.switch(source,pattern=pattern)
        self.assertEqual(result, expected)
    
    def test_declare_entities(self):
        """declare entities from a string"""
        entities = XCI.sgml.Entities.load("ISOamsc",path=XCI.sgml.ENTITIES_PATH / "custom.txt")
        result = {'<!ENTITY lpargt SDATA "&#xfffd;">', '<!ENTITY lceil SDATA "&#x2308;">', '<!ENTITY lfloor SDATA "&#x230a;">', '<!ENTITY rpargt SDATA "&#xe291;">', '<!ENTITY drcorn SDATA "&#x231f;">', '<!ENTITY dlcorn SDATA "&#x231e;">', '<!ENTITY urcorn SDATA "&#x231d;">', '<!ENTITY ulcorn SDATA "&#x231c;">', '<!ENTITY rfloor SDATA "&#x230b;">', '<!ENTITY rceil SDATA "&#x2309;">'}
        self.assertSetEqual(set(entities.declare(notation="sdata")),result)
    
    def test_fix_control_codes(self):
        """remove unsupported control characters entities from a string"""
        entities = XCI.sgml.Entities.load(path=XCI.sgml.ENTITIES_PATH / "control.txt")
        source = "<xml>Ce texte &#x3;contient &#x7;&#x3;des codes de contrôle&#x7;.</xml>"
        expected = "<xml>Ce texte ne contient pas de codes de contrôle.</xml>"
        result = entities.remove_from(source).replace(
            "contient des","ne contient pas de")
        self.assertEqual(result,expected)
    
    def test_fix_entities_names(self):
        """change control codes and entities"""
        # see also test_fix_document_contents in test_pipeline.
        entities = XCI.sgml.Entities.load(path=XCI.sgml.ENTITIES_PATH / "control.txt")
        source = "<p>&#x3;accès à la&#x7; messagerie électronique et l’« intranet » de l’entreprise&#x3;&#x7;. J'ajoute un cœur.</p>"
        result = entities.remove_from(source)
        self.assertEqual(result, "<p>accès à la messagerie électronique et l’« intranet » de l’entreprise. J'ajoute un cœur.</p>")
        result = XCI.xml.LX.tostring(XCI.xml.LX.XML(result.encode())) # LX.tostring() defaults to ASCII
        self.assertEqual(result.decode(), "<p>acc&#232;s &#224; la messagerie &#233;lectronique et l&#8217;&#171;&#8239;intranet&#8239;&#187; de l&#8217;entreprise. J'ajoute un c&#339;ur.</p>")
        entities = XCI.sgml.Entities.load()
        result = entities.rename_in(result.decode())
        self.assertEqual(result, "<p>acc&egrave;s &agrave; la messagerie &eacute;lectronique et l&rsquo;&laquo;&#8239;intranet&#8239;&raquo; de l&rsquo;entreprise. J'ajoute un c&oelig;ur.</p>")
    