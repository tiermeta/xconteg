"""{} document or directory contents processing."""
import sys
from xconteg import ui, Index, __version__

def demo():
    ui.LOG.info(str(__doc__).format('XConTeg '+__version__))
    if len(sys.argv)>1:
        index = Index(*sys.argv[1:], suffixes=(".xml",".sgml",))
        ui.require(len(index)>0, "No document to process.")
        for i in index:
            x = i.load()
            message = "is empty." if x.empty else f"is of type '{x.root.tag}'."
            ui.LOG.info(f"'{i.name}' "+message)
    else:
        ui.LOG.info("Try 'xconteg .' to process the current directory.")

if __name__ == "__main__": demo()
