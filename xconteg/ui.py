"""user interaction / interface"""

import io # proc() only
import logging as LOG
import sys, time # gauge only
import tkinter # Tk forms only
import os, shutil, subprocess, pathlib # subproc() only
import typing
#import xconteg as XCI # should be removed

LOG.basicConfig(format="%(asctime)s|%(levelname)s: %(message)s",
                datefmt="%j-%H:%M:%S", level=LOG.DEBUG, handlers=[
                    LOG.StreamHandler(),
                    LOG.FileHandler(f"{__package__}.log"),
                ])

def deprecated(message:str) -> typing.Callable:
    def decorator(func:typing.Callable):
        def wrapper(*args, **kwargs):
            LOG.warning(f"'{func.__name__}' is deprecated. {message}")
            return func(*args, **kwargs)
        return wrapper
    return decorator

def require(condition: bool, message:str|None) -> bool:
    """Terminate with a message if required condition is not met."""
    if condition is False:
        message = message or "Requirement is not met."
        LOG.error(message)
        raise SystemExit(message)
    return condition

def gauge(it, prefix="", size=40, out=sys.stdout, start=time.time()):
    """simple progressbar, scripted alternative to https://tqdm.github.io/
    
    thanks https://stackoverflow.com/a/34482761

    usage:
        for i in gauge(range(15), "Computing: ", 40):
            ...
    """
    length = len(it) if it.__len__ else None
    def increase(j):
        x = int(size*j/length)
        remaining = ((time.time() - start) / j) * (length - j)        
        mins, sec = divmod(remaining, 60) # limited to minutes
        print(f"{prefix}[{u'█'*x}{(' '*(size-x))}] {j}/{length}"
              f" | ETA {int(mins):02}:{sec:03.1f}"
              , end='\r', file=out, flush=True)
    if length:
        increase(0.1) # avoid div/0 
        for i, item in enumerate(it):
            yield item
            increase(i+1)
        print("\n", flush=True, file=out)
    else:
        animation, counter = "|/—\\", 0
        for item in it:
            print(animation[counter % len(animation)], end="\r")
            #print(".", sep="", end="", flush=True) # stupid countless gauge
            counter += 1
            yield item

def code_messages(messages:list[str]) -> dict:
    """convert a list of messages to a dict of int codes"""
    return {codenum : " ".join(message.split()) for codenum, message in zip(
        (int(c) for c in range(len(messages))),
        (m.replace("\n"," ").strip() for m in messages))}

def subproc(source:bytes, *cmd:str, cwd:pathlib.Path|None=None, **envar:str
            ) -> tuple[bytes|None, bytes|None, int]:
    """Process bytes from the command line and return output, errors, rcode."""
    env = os.environ.copy()
    for var in envar: env[var] = envar.get(var) or ""
    err, url = f"'{cmd[0]}' must be in your PATH.", env.get(f"{cmd[0]}_url")
    err = err + (f" Get it at {url}." if url else "")
    if require(shutil.which(cmd[0]) is not None, err) is False: LOG.error(err)
    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE, env=env, cwd=cwd)
    result: bytes|None
    error: bytes|None
    result, error = (i or None for i in p.communicate(source))
    return result, error, p.returncode

def proc(source:bytes, proc:typing.Callable,
               *args, encoding=None, gauge=False, **kwargs) -> bytes:
    """Process bytes with callable function."""
    streami = io.BytesIO(source).readlines()
    streami = gauge(streami) if gauge else streami
    streamo = io.BytesIO()
    for line in streami:
        line = line.decode(encoding=encoding) if encoding else line.decode()
        line = proc(line, *args, **kwargs)
        streamo.write(line.encode(encoding=encoding
                                  ) if encoding else line.encode())
    streamo.seek(0)
    return streamo.read()

class TkSimpleFields(list):
    """fields drawer and reader for simple tree (name, value)"""

    def __init__(self, tree) -> None:
        """"""
        self.master = tkinter.Tk()
        self.tree = tree
        self.title = tree.tag
        self.draw()
    
    def draw(self) -> None:
        """draw controls and bind events"""
        
        def save() -> None:
            """update tree with fields values"""
            for i,field in enumerate(self): self.tree[i].text=field.get()
            self.master.destroy()
        
        def add(field, position) -> None:
            """draw a field at given position"""
            label = tkinter.Label(self.master,text=field.tag)
            label.grid(row=position,sticky=tkinter.E,padx=(10,4))
            entry = tkinter.Entry(self.master,width=60,relief=tkinter.FLAT)
            entry.insert(0,field.text or "")
            entry.grid(row=position,column=1,sticky=tkinter.W,padx=(4,10))
            self.append(entry)
        
        for position, field in enumerate(self.tree): add(field, position)
        button = tkinter.Button(self.master,width=6,text="OK",command=save)
        button.grid(columnspan=2,pady=4,sticky=tkinter.S)
        self.master.title(self.title)
        self.master.resizable(False, False)
        self.master.protocol("WM_DELETE_WINDOW", self.master.destroy)
        self.master.mainloop()
    
    def __str__(self) -> str:
        """make human readable"""
        elements = lambda x: ": ".join([x.tag, x.text])
        return "\n".join(elements(x) for x in self.tree)


class TkSimpleInput:
    """minimalistic popup entry"""
   
    def __init__(self, title:str) -> None:
        """"""
        self.value:str|None = None
        self.title = title
        self.master = tkinter.Tk()
        self.draw()

    def draw(self) -> None:
        """draw controls and bind events"""

        def save(event) -> None: # event is sent by binding
            """save the value and quit"""
            self.value = self.entry.get()
            close()
        
        def close() -> None:
            """"""
            self.master.destroy()
            return None
        
        self.master.resizable(True,False)
        self.master.title(self.title)
        self.master.bind("<Escape>",close)
        self.master.protocol("WM_DELETE_WINDOW",close)
        self.entry = tkinter.Entry(self.master,width=len(self.title)*4)
        self.entry.pack(fill=tkinter.X,padx=(10,10),pady=(10,10))
        self.entry.focus()
        self.entry.bind("<Return>",save)
        self.master.mainloop()
    
    def __str__(self) -> str:
        """"""
        return self.value or ""
